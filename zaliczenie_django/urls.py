from django.conf.urls import url, include
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^oauth/disconnect/facebook/home/', views.home, name='home'),
    url(r'^$', views.home, name='home'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.home, name='logout'),
    url(r'^password/$', views.password, name='password'),
    url(r'^oauth/', include('social.apps.django_app.urls', namespace='social'))
]
