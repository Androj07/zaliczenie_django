from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AdminPasswordChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages
from django.shortcuts import render, redirect
import requests
from social.apps.django_app.default.models import UserSocialAuth

@login_required
def home(request):
    user = request.user
    context = {}
    url = 'http://api.zalando.com/articles'

    try:
        facebook_login = user.social_auth.get(provider='facebook')
    except UserSocialAuth.DoesNotExist:
        facebook_login = None

    if facebook_login:
        logged = user.first_name + ' ' + user.last_name
        can_disconnect = (user.social_auth.count() > 1 or user.has_usable_password())
        response = requests.get(url)
        data = response.json()
        context = {
            'facebook_login':facebook_login,
            'can_disconnect': can_disconnect,
            'zalandoItems':data['content'],
            'logged_as': logged
        }

    return render(request, 'home.html',context)


def login(request):
    return render(request, 'login/login.html')


@login_required
def password(request):
    if request.user.has_usable_password():
        PasswordForm = PasswordChangeForm
    else:
        PasswordForm = AdminPasswordChangeForm

    if request.method == 'POST':
        form = PasswordForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.success(request, 'Your password was successfully updated!')
            return redirect('password')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordForm(request.user)
    return render(request, 'password.html', {'form': form})